---
title: 编译原理
tags: programming
mathjax: true
---

## 编译阶段

编译的过程分为**词法分析、语法分析、语义分析、中间代码生成、机器无关代码优化、目标代码生成、机器相关代码优化**等几个步骤。词法分析、语法分析、语义分析、中间代码生成统一称为分析部分（前端），目标代码生成、机器相关代码优化被统一称为综合部分（后端）。
当语法分析、语义分析、中间代码生成三个部分合在一起时称为**语法制导翻译**。

## 词法分析

词法分析的主要任务是从左向右扫描源程序的字符，识别出各个单词，确定单词的类型，并将识别出的单词转换为统一的机内表示——词法单元（ token）形式。

## 自顶向下分析

从文法的开始符号开始进行分析的过程被称为自顶向下分析.

**最左推导**: 从文法开始符号的产生式中最左的符号开始替换的推导方式被称为最左推导。
最左推导对应的规约方式被称为**最右规约**。

**最右推导**: 从文法开始符号的产生式中最右的符号开始替换的推导方式被称为最右推导。
最右推导对应的规约方式被称为**最左规约**。

**递归下降分析**:
递归下降解析器由一组相互递归的程序（或等价的非递归程序）构建而成，其中每个程序都实现了文法中的一个非终结符。


**左递归**:
若 $A \Rightarrow^{*} Au$, $u \in (T\cup N)^{*}$, 则称该产生式是左递归的. 含有左递归产生式的文法被称为左递归文法. 左递归文法会使递归下降解析器陷入无限循环.

**左递归的消除**:

通过引入非终结符和空产生式将**直接左递归**转化为右递归;

如下算法消除**间接左递归**:
  
  1. sort all non-terminal $N_1, N_2, \cdots, N_k$;

  2. $\forall i= 1,\cdots,k$: 

      - $\forall j = 1,\cdots, i-1$: 

          - rewrite $A_i\to A_j\gamma$ into $A_i \to \delta_1\gamma|\cdots|\delta_k\gamma$, where $A_j\to\delta_1|\cdots|\delta_k$;

      - eliminate directly left-recursion.


### 几类重要集合

**First 集**是指文法 $G = (T, N, P, S)$ 中文法符号串 $\alpha \in T\cup N$ 所能推导出的所有串的第一个终结符构成的集合, 记为 $\mathsf{First}(\alpha)$. 

如果 $\alpha \Rightarrow^{*}  \epsilon$, 则 $\epsilon \in \mathsf{First}(\alpha)$.

对于串 $\alpha \to \alpha_1\alpha_2\cdots\alpha_n \in G$, 计算 $\mathsf{First}(\alpha)$ 的算法为:

  0. if $\alpha \in T$, return $\mathsf{First}(\alpha) = \{\alpha\}$;

  1. $X \leftarrow \{\}$, $\varphi \leftarrow \mathsf{true}$;

  2. forall $i \leftarrow 1, 2, \cdots, n$:
      
      - $X \leftarrow X \cup \mathsf{First}(\alpha_i)$;

      - $\alpha_i \not\Rightarrow^{*} \epsilon$ ? $\varphi \leftarrow \mathsf{false}$, break;

  5. $\varphi = \mathsf{false}$ ? $X \leftarrow X - \{\epsilon\}$;

  6. return $\mathsf{First}(\alpha) = X$.


**Follow 集**是指文法 $G = (T, N, P, S)$ 中非终结符 $A \in N$ 能紧跟的终结符构成的集合, 记为 $\mathsf{Follow}(A)$. 
$$
\mathsf{Follow}(A) = \{ a \in T | S \Rightarrow^{*} \alpha A a \beta,\ \alpha,\beta \in (T\cup N)^{*} \}.
$$

如果 $A$ 是某个句型的最右符号, 则结束符号 $\mathsf{\$} \in \mathsf{Follow}(A)$.

计算非终结符 $\mathsf{Follow}(A)$ 的算法为:

  0. $X \leftarrow \{\}$;

  1. forall $p \in P$:

      - if $p$ is like $A \to \alpha B$, $X \leftarrow X \cup \mathsf{Follow}(B)$;

      - elif $p$ is like $A \to \alpha B \beta \land \epsilon\in\mathsf{First}(\beta)$, $X \leftarrow X \cup \mathsf{Follow}(B)$;

      - elif $p$ is like $C \to \varphi A \gamma$, $X \leftarrow X \cup \mathsf{First}(\gamma)$;

      - else continue;

  2. return $\mathsf{Follow}(A) = X$.


计算文法 $G$ 的全部 Follow 集的算法为:

  1. forall $p \in P$, supose $p$ is like $A \to \alpha_1\alpha_2\cdots\alpha_n$: 
      - $\mathsf{Follow}(\alpha_n)  \leftarrow \mathsf{Follow}(\alpha_i) \cup \mathsf{Follow}(A)$; 
      - forall $i \leftarrow 1, 2, \cdots, n-1$: 
          - $\mathsf{Follow}(\alpha_i) \leftarrow \mathsf{Follow}(\alpha_i) \cup \mathsf{First}(\alpha_{i+1}\alpha_{i+2}\cdots\alpha_n) - \{\epsilon\}$;

  2. repeate #1 until no changes.


**可选集**是指选用产生式 $A\to\beta$ 推导时对应的输入符号的集合，记为 $\mathsf{Select}(A\to\beta)$.

从而对于产生式 $A\to\beta$, 有
$$
\mathsf{Select}(A\to\beta) = \left\{
\begin{aligned}
&\mathsf{First}(\beta), &\epsilon\not\in \mathsf{first}(\beta), \\
&(\mathsf{First}(\beta) - \{\epsilon\}) \cup \mathsf{Follow}(A), &\epsilon \in \mathsf{first}(\beta).
\end{aligned}
\right.
$$

### LL(1) 文法

即从左(left)向右扫描的最左(left)推导文法, 且有一个(1) 前看符号.


称文法 $G = (T, N, P, S)$ 是 **LL(1) 文法**, 当且仅当 $G$ 的任意两个具有相同左部的产生式 $A \to \alpha | \beta$ 满足：

  1. 若 $\alpha \not\Rightarrow^{*} \epsilon \land \beta \not\Rightarrow^{*} \epsilon$, 则 $\mathsf{First}(\alpha) \cap \mathsf{First}(\beta) = \varnothing$;

  2. $\alpha$ 和 $\beta$ 至多有一个推导出 $\epsilon$;

  3. $\forall x\in \{\alpha, \beta\}$, 若 $x\Rightarrow^{*} \epsilon$ 则 $\mathsf{First}(\alpha) \cap \mathsf{Follow}(A) = \varnothing$.


### 表驱动的预测分析法

使用 PDA 进行分析预测的方式即为表驱动的预测分析法，也称为非递归的预测分析法.

首先根据文法 $G = (T, N, P, S)$ 的产生式构造分析表一个 $N \times T \to P$ 的表 $M$:
$$M(A, t) = \left\{\begin{aligned} 
&A \to \alpha,\quad t\in\mathsf{Select}(A\to\alpha), \\ 
&\mathsf{empty}, \quad \text{otherwise}. 
\end{aligned}\right.$$

然后设置驱动程序为:

  - 输入: 符号串 $w$ 和文法 $G$ 的分析表 $M$;

  - 输出: 若 $w \in L(G)$, 则输出 $w$ 的最左推导，否则给出错误指示;

  - 设 $A = (T, Y, S, \Gamma, \delta, \lambda)$ 为驱动程序采用的 PDA, 则算法为: 

      0. $\gamma \in \Gamma$ be current stack series, $i\leftarrow 0$;

      1. while $\gamma \ne \epsilon$:

          - if $\gamma_i = w_i$:

              - $\gamma \leftarrow \gamma_{_{0:i-1}}$;

              - $i \leftarrow i + 1$;

          - elif $\gamma_i \in T$:

              - raise error;

          - elif $M(\gamma_i, w_i) = \mathsf{empty}$:

              - raise error;

          - elif $M(\gamma_i, w_i) = \gamma_i \to \alpha_1\alpha_2\cdots\alpha_n$:

              - output $\gamma_i \to \alpha$;

              - $\gamma \leftarrow \gamma_{_{0:i-1}}\alpha_n\alpha_{n-1}\cdots\alpha_1$.

即可进行表驱动的预测分析.

*注意 $\gamma$ 的切分方向为 $\gamma = \gamma_{_{0:i-1}}\gamma_{i}$. 且产生式入栈时需要逆序压栈, 即 $\gamma$ 的尾部为栈顶，头部为栈底. 其中 $\alpha_i \in T\cup N$.*

表驱动预测分析法的步骤：

  1. 消除二义性, 消除左递归;

  2. 提取左公因子，消除回溯;

  3. 求 $\mathsf{First}, \mathsf{Follow}, \mathsf{Select}$ 集合;

  4. 若 $\mathsf{Select}$ 有交集，则非 LL(1) 文法，无法进行自顶向下分析; 否则

  5. 根据 $\mathsf{Select}$ 集合求出预测分析表;

  6. 使用驱动程序进行预测分析.


## 自底向上分析

从输入串 $w$ 规约为文法开始符号 $S$ 的过程被称为自底向上分析.

**移入-规约分析**时自底向上分析普遍采用的分析形式.

移入-规约分析的下推自动机:

  - $A = (X, Y, Z, \Gamma, \delta, \lambda)$ be a PDA:

      - $X = T$;

      - $Z = \{s_i\}$;

      - for $\delta$:

          - $\delta(s_0, \epsilon, w_0) = \{(s_1, w_0)\}$;

          - $\delta(s_i, \gamma, w_k) = \{(s_i, \gamma w_k)\}$, if no reduction;

          - $\delta(s_i, \gamma, \epsilon) = \{(s_{i + 1}, \gamma_{_{0:k-1}}A)\}$, if $\exists A \to \gamma_{_{k:n}}$;

          - $\delta(s_i, \gamma A, \epsilon) = \{(s_{j}, \gamma A)\}$, if $A \in N$, where $s_j$ is a state $A$ has been analyzed;

          - $\delta(s_i, \gamma, \$) = \{(s_n, S)\}$, if $\exists S \to \gamma$.

          - otherwise $\varnothing$.

      - $\lambda(s_n, S) = \mathsf{accepted}$, otherwise $\mathsf{denied}$.


**项**是指文法 $G$ 的某个产生式 $A \to \alpha\beta$ 加上右部某位置上的一点形成的表达式 $[A \to \alpha\cdot\beta]$, 该点代表栈顶指向的当前位置, 说明分析器已经识别到该产生式的前缀.

**增广文法(Augmented Grammar)**是指在 $G = (T, N, P, S)$ 中加入一个新的开始符号 $S'$ 和新的产生式 $S' \to S$ 得到的文法 $G' = (T, N, P\cup\{S' \to S\}, S')$.

**项目闭包**是指项目集合 $I$ 通过 $\epsilon$ 即可到达的所有可能的状态, 记为 $\mathsf{Closure}(I)$.

项目闭包的计算算法为:

  0. $C \leftarrow I$;

  1. $\forall A\to \alpha\cdot B\beta \in C$, $\forall B\to\gamma \in P\cup\{[S'\to S]\}$:

      - if $B\to\cdot\gamma \not\in C$:

          - $C \leftarrow C\cup\{[B\to\cdot\gamma]\}$;

  2. repeat #1 until no new item added to $C$.


Goto 集合是指状态集合 $I$ 对栈顶符号 $X\in T\cup N$ 的集合 $\mathsf{Goto}(I, X) := \mathsf{Closure}\left(\left\{[A \to \alpha X\cdot \beta] | [A \to \alpha \cdot X \beta] \in I \right\}\right)$, 即在状态 $I$ 将 $X$ 压栈之后能进入的状态集合. 

*显然，我们要求每一个状态 $s_i$, 都必须只能有 $|\mathsf{Goto}(s_i, X)| \le 1$.*

### LR(0) 文法

即从左(left)向右扫描的最右(right)推导的文法, 且向前零个(0)符号.

LR(0) 分析器就是上述移入-规约分析的下推自动机, 当 $s_j = \mathsf{Goto}(s_i, X)$ 不为空时:

  - 若 $X \in T$, $\delta(s_i, \gamma , X) = (s_i, \gamma X)$;

  - 若 $X \in N$, $\delta(s_i, \gamma X, epsilon) = (s_j, \gamma X)$.

而对于 LR(0) 的规约，则为 $\delta(s_i, \gamma\alpha_1\alpha_2\cdots\alpha_n, \epsilon) = \{(s_j, \gamma A)\}$, if $\exists A \to \alpha_1\alpha_2\cdots\alpha_n$.

因而 LR(0) 只能处理前缀不同的规约规则.

### SLR(1) 文法

即从左(left)向右扫描的最右(right)推导的文法, 且向前看一个(1)符号, 采用极简(S)的前看方式.

SLR(1) 的自动机的栈被修改为$\Gamma\times T$, 其中的元素表示为 $\gamma[h]$, 即当前栈为 $\gamma$, 前看符号为 $h$. 

SLR(1) 的 $\mathsf{Goto}$ 动作较 $LR(0)$ 没有变化, 前看符号并不会影响 $\mathsf{Goto}$ 动作, 但是其规约规则被改进为:

$\delta(s_i, \gamma\alpha_1\alpha_2\cdots\alpha_n[t], \epsilon) = \{(s_j, \gamma A)\}$, if $\exists A \to \alpha_1\alpha_2\cdots\alpha_n \land t\in \mathsf{Follow}(A)$.

SLR(1) 能处理有相同前缀的情况，但是却不能处理一个规则有不同后继的情况.

### LR(1) 文法

即从左(left)向右扫描的最右(right)推导的文法, 且向前看一个(1)符号.

LR(1) 根据产生式的 $\mathsf{Follow}$ 集为每一个项目都派遣前看符号，即
$[A\to\alpha\cdot B\beta, a] \in I \times T$ iif. $a \in \mathsf{Follow}(A)$.

同时，项目集闭包的算法也有所不同:

  0. $C \leftarrow I$;

  1. $\forall [A\to \alpha\cdot B\beta, a] \in C$, $\forall B\to\gamma \in P\cup\{[S' \to S, \$]\}$:

      - $\forall b \in \mathsf{First}(\beta a)$:

          - if $[B\to\cdot\gamma, b] \not\in C$:

              - $C \leftarrow C\cup\{[B\to\cdot\gamma, b]\}$;

  2. repeat \#1 until no new item added to $C$.

同样，$\mathsf{Goto}(I, X) := \mathsf{Closure}\left(\left\{[A \to \alpha X\cdot \beta, a] | [A \to \alpha \cdot X \beta, a] \in I \right\}\right)$, 其中 $X \in N\cup T$.

LR(1) 分析器也不会改变对 $\mathsf{Goto}$ 的动作, 当 $s_j = \mathsf{Goto}(s_i, X)$ 不为空时:

  - 若 $X \in T$, $\delta(s_i, \gamma[a] , X) = (s_i, \gamma X[a])$;

  - 若 $X \in N$, $\delta(s_i, \gamma X[a], \epsilon) = (s_j, \gamma X[a])$.

而对于 LR(1) 的规约，则为 $\delta(s_i, \gamma\alpha_1\alpha_2\cdots\alpha_n[a], \epsilon) = \{(s_{j_a}, \gamma A[a])\}$, if $\exists A \to \alpha_1\alpha_2\cdots\alpha_n \land a\in \mathsf{Follow}(A)$.

即后继状态 $s_{j_a}$ 依赖于前看符号 $a$.

由于后继状态依赖于前看符号，因此 LR(1) 能处理不同后缀的文法，但是也正因此，LR(1) 的状态数量急剧膨胀.

### LALR(1) 文法

为解决 LR(1) 的状态膨胀的问题，LALR(1) 引入合并同心项目集合的操作.

若 $[A\to\gamma\cdot, a] \in I_1, [A\to\gamma\cdot, b] \in I_2$，则称 $I_1$ 和 $I_2$ 具有相同核心.

将具有相同核心的状态合并为同一个状态的操作，则称为合并同心项目集.

将 LR(1) 分析表进行合并同心项目集即可得到 LALR(1). 

但是合并同心项目集合有可能导致归约-归约冲突, 这是因为在合并时忽略了前看符号.

*仅合并具有相同项目 $[A\to\gamma\cdot, a]$ 的状态不会导致归约-归约冲突.*

